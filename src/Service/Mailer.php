<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Nelmio\Alice\scenario3\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Symfony\Component\Mime\Address;

/**
 * Class Mailer
 */
class Mailer
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var object|User
     */
    private $user;

    /**
     * @var Environment
     */
    private $render;

    /**
     * @var string
     */
    private $email = '';

    public function __construct(MailerInterface $mailer, TokenStorageInterface $tokenStorage, Environment $environment, string $email)
    {
        $this->mailer = $mailer;
        $this->render = $environment;
        $this->email = $email;

        if ($tokenStorage->getToken()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * @param $fromEmail
     * @param UserInterface|User|string $user
     * @param $subject
     * @param $body
     */
    public function sendMessage($fromEmail, $user, $subject, $body): void
    {
        $mail = new Email();

        $mail->from(new Address($fromEmail, 'Vetvite'));
        $mail->to($user instanceof User ? $user->getEmail() : $user);
        $mail->subject($subject);
        $mail->html($body);
        $mail->replyTo(new Address($fromEmail, 'Vetvite'));

        try {
            $this->mailer->send($mail);
        } catch (TransportExceptionInterface $e) {
        }
    }

    public function sendConfirmationEmail(User $user):void
    {
        $template = $this->render->render('emails/confirm.html.twig', [
            'user' => $user
        ]);

        $subject = "Confirme ton compte {$user->getFirstName()} !";

        $this->sendMessage($this->email, $user, $subject, $template);
    }

    public function sendForgotEmail(User $user, string $url):void
    {
        $template = $this->render->render('emails/forgot.html.twig', [
            'user' => $user,
            'url' => $url
        ]);

        $subject = "Vous avez perdu votre mot de passe {$user->getFirstName()} ?";

        $this->sendMessage($this->email, $user, $subject, $template);
    }

    public function sendPasswordChanged(User $user, string $plainPassword):void
    {
        $template = $this->render->render('emails/passwordUpdated.html.twig', [
            'user' => $user,
            'plainPassword' => $plainPassword,
        ]);

        $subject = "Votre mot de passe à bien été modifié {$user->getFirstName()} !";

        $this->sendMessage($this->email, $user, $subject, $template);
    }

    public function sendEmailChanged(User $user, string $oldEmail, string $newEmail):void
    {
        $template = $this->render->render('emails/emailUpdated.html.twig', [
            'user' => $user,
            'oldEmail' => $oldEmail,
            'newEmail' => $newEmail,
        ]);

        $subject = "Votre adresse email à bien été modifiée {$user->getFirstName()} !";

        $this->sendMessage($this->email, $newEmail, $subject, $template);
        $this->sendMessage($this->email, $oldEmail, $subject, $template);
    }

    public function sendGDPRData(User $user)
    {
        $template = $this->render->render('emails/gdprRequest.html.twig', [
            'user' => $user,
        ]);

        $subject = "Demande des données RGPD";

        $this->sendMessage($this->email, $this->email, $subject, $template);
    }
}
