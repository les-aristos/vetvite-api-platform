<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/user")
 */
class AccountController extends AbstractController
{
    private $request;

    public function __construct(RequestStack $stack)
    {
        $this->request = $stack->getCurrentRequest();
    }

    private function initialize(): array
    {
        $response = null;

        if ($this->request->getContentType() != 'json' || !$this->request->getContent()) {
            $response = new Response('', 400);
        }

        $data = json_decode($this->request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $response = new JsonResponse('invalid json body: ' . json_last_error_msg(), 400);
        }

        return [$response, $data];
    }

    /**
     * @Route(name="forgot_password", path="/forgot", methods={"POST"})
     */
    public function forgot(Mailer $mailer, EntityManagerInterface $entityManager): Response
    {
        list($error, $data) = $this->initialize();
        if ($error) {
            return $error;
        }

        $email = $data['email'] ?? '';

        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse('Adresse email invalide !', 400);
        }

        if (! ($user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email])) instanceof User) {
            return new JsonResponse('Adresse email introuvable !', 400);
        }

        if (! ($url = $data['url'] ?? false)) {
            return new JsonResponse('Requete invalide !', 400);
        }

        $user->regenerateToken();

        try {
            $entityManager->flush();
            $mailer->sendForgotEmail($user, $url);
            return new JsonResponse(null, 200);
        } catch (\Exception $e) {
            return new JsonResponse($e, 400);
        }
    }

    /**
     * @Route(name="reset_password", path="/reset", methods={"PATCH"})
     */
    public function reset(Mailer $mailer, EntityManagerInterface $entityManager, ValidatorInterface $validator): Response
    {
        list($error, $data) = $this->initialize();
        if ($error) {
            return $error;
        }

        $token = $data['token'] ?? '';
        $plainPassword = $data['plainPassword'] ?? '';

        if (! ($user = $entityManager->getRepository(User::class)->findOneBy(['token' => $token])) instanceof User) {
            return new JsonResponse('Token invalide !', 400);
        }

        $user->setPassword('')->setPlainPassword($plainPassword);

        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }

        try {
            $entityManager->flush();

            $user->regenerateToken();
            $entityManager->flush();
            return new JsonResponse(['email' => $user->getEmail()], 200);
        } catch (\Exception $e) {
            return new JsonResponse($e, 400);
        }
    }

    /**
     * @Route(name="confirm_account", path="/confirm", methods={"POST"})
     */
    public function verify(EntityManagerInterface $entityManager): Response
    {
        list($error, $data) = $this->initialize();
        if ($error) {
            return $error;
        }

        $user = $this->getUser();

        if (str_starts_with(strtolower($user->getToken()), strtolower($data['code']))) {
            try {
                $user->setEnabled(User::SET_ENABLED);
                $entityManager->flush();
                dump($user->getEnabled());

                return new JsonResponse(['newState' => $user->getEnabled()], 200);
            } catch (\Exception $e) {
                return new Response('Confirmation de votre compte impossible !', 400);
            }
        }

        return new Response('Code invalide !', 400);
    }

    /**
     * @Route(name="resend_confirm_email", path="/sendconfirm", methods={"POST"})
     */
    public function resend_confirm(Mailer $mailer, EntityManagerInterface $entityManager): Response
    {
        list($error) = $this->initialize();
        if ($error) {
            return $error;
        }

        $user = $this->getUser();

        try {
            if ($user->getEnabled() === User::SET_WAITING) {
                $user->regenerateToken();
                $entityManager->flush();
                $mailer->sendConfirmationEmail($user);
                return new Response(null, 200);
            }

            return new Response('Compte déjà vérifié !', 400);
        } catch (\Exception $e) {
            return new Response('Envoi du code impossible !', 400);
        }
    }

    /**
     * @Route(name="download_data", path="/gdpr/download", methods={"POST"})
     */
    public function download_data(Mailer $mailer): Response
    {
        list($error) = $this->initialize();
        if ($error) {
            return $error;
        }

        $user = $this->getUser();

        try {
            $mailer->sendGDPRData($user);
            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response('Erreur lors de la demande !', 400);
        }
    }
}
