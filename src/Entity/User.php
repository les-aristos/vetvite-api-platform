<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\FullTextByWordsSearchFilter;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     mercure=true,
 *     collectionOperations={
 *         "get",
 *         "post",
 *         "get_user_infos"={"normalization_context"={"groups"={"user:infos"}}, "method"="GET", "path"="/users/infos"}
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"user:read"}, "enable_max_depth"="true"}
 *         },
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *     },
 *     normalizationContext={"groups"={"user:read"}, "enable_max_depth"="true"},
 *     denormalizationContext={"groups"={"user:write"}, "enable_max_depth"="true"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "email": "exact", "phoneNumber": "exact", "roles": "partial", "token": "exact"})
 * @ApiFilter(DateFilter::class, properties={"createdAt"})
 * @ApiFilter(NumericFilter::class, properties={"enabled"})
 * @ApiFilter(FullTextByWordsSearchFilter::class, properties={
 *              "lastName": "partial",
 *              "firstName": "partial",
 *              "job": "partial",
 *              "offices.name": "partial",
 *              "offices.postalCode": "start",
 *              "offices.city": "partial",
 *              "offices.address": "partial",
 *           })
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user_account")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="L'adresse email est déjà utilisée !"
 * )
 */
class User implements UserInterface
{
    public const SET_ENABLED = 1;
    public const SET_DISABLED = -99;
    public const SET_WAITING = 0;
    public const SET_BANNED = -1;

    public const IS_MEN = 'men';
    public const IS_WOMEN = 'women';
    public const IS_OTHER = 'other';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read","appointment:read","office:read","care:read","user:infos"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Email
     * @Assert\Length(min=5, max=255)
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     * @Groups({"user:read","admin:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(min=2, max=16)
     * @Assert\Choice({"men", "women", "other"})
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read", "user:infos"})
     */
    private $gender;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotNull
     * @Groups({"admin:write","user:read","appointment:read","office:read","care:read","user:infos"})
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank
     * @Assert\Length(min=2, max=64)
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank
     * @Assert\Length(min=2, max=64)
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $dob;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max=64)
     * @Groups({"admin:write","user:read"})
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=24)
     * @Assert\NotBlank
     * @Assert\Length(max=24)
     * @Groups({"user:read","user:write","appointment:read","office:read","care:read","user:infos"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user:read","user:infos"})
     */
    private $createdAt;

    /**
     * @Assert\Length(min=8)
     * @Groups({"user:write"})
     */
    private $plainPassword;

    // For Pro

    /**
     * @ORM\OneToMany(targetEntity=Care::class, mappedBy="pro", orphanRemoval=true)
     * @Groups({"user:read","admin:write"})
     * @MaxDepth(1)
     */
    private $cares;

    /**
     * @ORM\ManyToMany(targetEntity=Office::class, inversedBy="pros")
     * @Groups({"user:read","admin:write"})
     * @MaxDepth(2)
     */
    private $offices;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     * @Assert\Url
     * @Groups({"user:read","user:write","appointment:read","office:read","user:infos"})
     */
    private $avatar;

    // For Client

    /**
     * @ORM\OneToMany(targetEntity=Appointment::class, mappedBy="client")
     * @Groups({"user:read","admin:write"})
     * @MaxDepth(1)
     */
    private $appointments;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max=64)
     * @Groups({"user:read","user:write","office:read","user:infos"})
     */
    private $job;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=510)
     * @Groups({"user:read","user:write","user:infos"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=76, nullable=true)
     * @Assert\Length(max=76)
     * @Groups({"user:read","user:write","user:infos"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     * @Assert\Length(max=14)
     * @Groups({"user:read","user:write","user:infos"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     * @Groups({"user:read","user:write","user:infos"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     * @Groups({"user:read","user:write","user:infos"})
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=Office::class, mappedBy="creator")
     * @Groups({"user:read","admin:write"})
     * @MaxDepth(1)
     */
    private $createdOffices;

    /**
     * Used to automatically add user role
     * @Groups({"user:write"})
     */
    public $type;

    // Methods

    public function __construct()
    {
        $this->enabled = self::SET_WAITING;
        $this->addRole('ROLE_USER');

        $this->regenerateToken();
        $this->setCreatedAt(new \DateTime());

        $this->cares = new ArrayCollection();
        $this->offices = new ArrayCollection();
        $this->appointments = new ArrayCollection();
        $this->createdOffices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $password): self
    {
        $this->plainPassword = $password;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function addRole($role): self
    {
        if (!in_array($role, $this->roles, false)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    public function removeRole($role): self
    {
        foreach ($this->roles as $k => $r) {
            if ($r === $role) {
                unset($this->roles[$k]);
            }
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return (string)$this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function regenerateToken(): self
    {
        $this->setToken(sha1(uniqid('', true)));

        return $this;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEnabled(): int
    {
        return $this->enabled;
    }

    public function setEnabled(int $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(string $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return Collection|Care[]
     */
    public function getCares(): Collection
    {
        return $this->cares;
    }

    public function addCare(Care $care): self
    {
        if (!$this->cares->contains($care)) {
            $this->cares[] = $care;
            $care->setPro($this);
        }

        return $this;
    }

    public function removeCare(Care $care): self
    {
        if ($this->cares->removeElement($care)) {
            // set the owning side to null (unless already changed)
            if ($care->getPro() === $this) {
                $care->setPro(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Office[]
     */
    public function getOffices(): Collection
    {
        return $this->offices;
    }

    public function addOffice(Office $office): self
    {
        if (!$this->offices->contains($office)) {
            $this->offices[] = $office;
        }

        return $this;
    }

    public function removeOffice(Office $office): self
    {
        $this->offices->removeElement($office);

        return $this;
    }

    /**
     * @return Collection|Appointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setClient($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            // set the owning side to null (unless already changed)
            if ($appointment->getClient() === $this) {
                $appointment->setClient(null);
            }
        }

        return $this;
    }

    public function setUserHasPro(bool $_): self
    {
        $this->addRole('ROLE_PRO');
        return $this;
    }

    public function setUserHasAdmin(bool $_): self
    {
        $this->addRole('ROLE_ADMIN');
        return $this;
    }

    public function setUserHasClient(bool $_): self
    {
        $this->addRole('ROLE_CLIENT');
        return $this;
    }

    /**
     * @return Collection|Office[]
     */
    public function getCreatedOffices(): Collection
    {
        return $this->createdOffices;
    }

    public function addCreatedOffice(Office $createdOffice): self
    {
        if (!$this->createdOffices->contains($createdOffice)) {
            $this->createdOffices[] = $createdOffice;
            $createdOffice->setCreator($this);
        }

        return $this;
    }

    public function removeCreatedOffice(Office $createdOffice): self
    {
        if ($this->createdOffices->removeElement($createdOffice)) {
            // set the owning side to null (unless already changed)
            if ($createdOffice->getCreator() === $this) {
                $createdOffice->setCreator(null);
            }
        }

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
